void SerialReceive()
{
  while (Serial.available())
  {
    Serial.print("have "); ////////////
    byte temp = Serial.read();
    Serial.print(temp); ///////////////

    if (temp == 253) // stop byte
    {
      Serial.println("done reading"); ///////////////
      if (address == 0)
      {
        display_data();
        digitalWrite(LED_PIN, HIGH);
      }
      else
      {
        SerialSend();
      }
      read_cycle = 0;
    }

    if (read_cycle == 0)
    {
      if (temp == 254) // startbyte
      {
        read_cycle++;
      }
    }

    else if (read_cycle == 1)
    {
      address = temp;
      read_cycle++;
    }

    else if (read_cycle == 2)
    {
      if (temp % 2) print_big = true;
      else print_big = false;
      read_cycle++;
    }

    else
    {
      data[read_cycle - 3] = temp;
      data_length = read_cycle - 2;
      read_cycle++;
      break;
    }
    
    Serial.println(); /////////////////
    Serial.println(read_cycle); //////////////
  }

}

void SerialSend()
{
  // send data to next
  byte new_address = address - 1;

  mySerial.write(START_BYTE);
  //mySerial.write(format_byte);
  mySerial.write(new_address);
  for (byte i = 0; i < data_length; i++) mySerial.write(data[i]);
  mySerial.write(STOP_BYTE);
}


