// controls a ws2810 ledstrip over Serial. 
// first byte is checkbyte, must be 48. then its index, r, g, b.

const bool DEBUG_MODE = false;
byte CORRECT_CHECK_BYTE = 255;

#include <Adafruit_NeoPixel.h>

const int PIN = 6;
const int AMOUNT = 128;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(AMOUNT, PIN, NEO_GRB + NEO_KHZ800);

int led_write_delay = 50; 

long last_showed = millis();

void setup() {
  
  Serial.begin(115200);           // start serial for output
  Serial.println("ready");
  
  strip.begin();
  
  for(int i = 0; i < AMOUNT; i++) // blink the strip
  {
    strip.setPixelColor(i, strip.Color(50, 50, 100));
  }
  strip.show();
  delay(50);
  
  for(int i = 0; i < AMOUNT; i++)
  {
    strip.setPixelColor(i, strip.Color(0, 0, 0));
  }
  strip.show(); // Initialize all pixels to 'off'
}

byte i = 0;
byte r = 0;
byte g = 0;
byte b = 0;

void loop() {
  //delay(1);
  if(Serial.available() >= 5)
  //while(Serial.available())
  {
    byte checkbyte = Serial.read();
    if(checkbyte == CORRECT_CHECK_BYTE) // if correct checkbyte received go on receiving
    {
      i = Serial.read();
      r = Serial.read();
      g = Serial.read();
      b = Serial.read();
    }
    
    if(DEBUG_MODE)
    {
    Serial.print(checkbyte);
    Serial.print(" ");
    Serial.print(i);
    Serial.print(" ");
    Serial.print(r);
    Serial.print(" ");
    Serial.print(g);
    Serial.print(" ");
    Serial.print(b);
    Serial.println();
    }

    if(checkbyte == CORRECT_CHECK_BYTE)
    {
      strip.setPixelColor(i, strip.Color(r, g, b));
    }   
  }

  if(last_showed + led_write_delay < millis())
  {
    last_showed = millis();
    //Serial.println("showLed");
    strip.show();
  }
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()

