/**
  This sketch acts as raspi serial pin extension
  It read midi data from Midi Instrument on Serial1 and passes it on to Raspi over USB serial
*/

/*
 * fix cmd- values:
 * 
 * 
 *  message initializers
 *  251 - button down
 *  252 - button up
 *  253 - analog value
 *  254 - midi note
 *  255 - midi button 
 * 
 *  mnessage terminator
 *  250
 * 
 */

//////////////////////////////// buttons ////////////////////////

/* Dependencies */
#include <Wire.h>    // Required for I2C communication
#include "PCF8575.h" // Required for PCF8575

const byte address[] = {0x24, 0x25, 0x26, 0x27};

bool button_state[64];
bool button_state_prev[64];

byte midi_note;
byte midi_vel;



//////////////////////** PCF8575 instance *//////////////////////
PCF8575 expander0;
PCF8575 expander1;
PCF8575 expander2;
PCF8575 expander3;

/////////////////////////// analog ins //////////////////////////
int analog_state[6];
int analog_state_prev[6];

/////////////////////////// MIDI data ///////////////////////////

byte midi_index;;
byte midi_velocity;

//////////////////////////** setup() *//////////////////////////// 
void setup() {

  /* Setup serial for debug */
  Serial.begin(115200);

  //Serial.print("starting");

  // init midi port
  Serial1.begin(31250);

  /* Start I2C bus and PCF8575 instance */
  expander0.begin(address[0]);
  expander1.begin(address[1]);
  expander2.begin(address[2]);
  expander3.begin(address[3]);

  //Serial.println("reached.");

  /* Setup some PCF8575 pins for demo */
  for (int i = 0; i < 16; i++)
  {
    expander0.pinMode(i, INPUT_PULLUP);
    expander1.pinMode(i, INPUT_PULLUP);
    expander2.pinMode(i, INPUT_PULLUP);
    expander3.pinMode(i, INPUT_PULLUP);
  }

  for (int i = 0; i < 64; i++)
  {
    button_state_prev[i] = false;
  }

  //Serial.println("setup completed.");
}


/** loop() */
void loop() {

  // read all i2c expander buttons
  read_buttons(expander0, 0);
  read_buttons(expander1, 1);
  read_buttons(expander2, 2);
  read_buttons(expander3, 3);

  // read all adcs
  read_analog_ins();

  // read midi
  read_midi();
  
  //Serial.println("go");

  delay(40);
}

void read_buttons(PCF8575 expander, int index)
{
  for (int i = 0; i < 16; i++)
  {
    button_state[index * 16 + i] = !expander.digitalRead(i);

    if (button_state[index * 16 + i] != button_state_prev[index * 16 + i] && button_state[index * 16 + i])
    {
      button_state_prev[index * 16 + i] = button_state[index * 16 + i];
      Serial.write(251); // button down initializer
      Serial.write(index * 16 + i);
      Serial.write(250); // terminator
    }

    else if (button_state[index * 16 + i] != button_state_prev[index * 16 + i] && !button_state[index * 16 + i])
    {
      button_state_prev[index * 16 + i] = button_state[index * 16 + i];
      Serial.print(252); // button up initiializer
      Serial.write(index * 16 + i);
      Serial.write(250); // terminator
    }
  }
}

void read_analog_ins()
{
  // read..
  int a0 = analogRead(0);
  int a1 = analogRead(1);
  int a2 = analogRead(2);
  int a3 = analogRead(3);
  int a4 = analogRead(4);
  int a5 = analogRead(5);
  
  // map..
  analog_state[0] = map(a0, 0, 1023, 0, 249);
  analog_state[1] = map(a1, 0, 1023, 0, 249);
  analog_state[2] = map(a2, 0, 1023, 0, 249);
  analog_state[3] = map(a3, 0, 1023, 0, 249);
  analog_state[4] = map(a4, 0, 1023, 0, 249);
  analog_state[5] = map(a5, 0, 1023, 0, 249);

  // ..and write to serial
  for (int i = 0; i < 6; i++)
  {
    if(abs(analog_state[i] - analog_state_prev[i]) > 5)
    {
      analog_state_prev[i] = analog_state[i];
      Serial.write(253); /// starting char for analogreading message: 253
      Serial.write(i);  // then comes the index
      Serial.write(analog_state[i]); // then the value
      Serial.write(250); // terminator
    }
  }
}

void read_midi()
{
  if (Serial1.available()) {

    byte inByte = Serial1.read();

    if (inByte == 0x90) // note on command for channel 0
                        // only listening to note on commands for now
    {
      //Serial.print("noteon ");
      delayMicroseconds(550);

      midi_note = Serial1.read();
      midi_vel = Serial1.read();

      Serial.write(254); // midi note initialiser
      Serial.write(midi_note);
      Serial.write(midi_vel);
      Serial.write(250); // terminator

      //Serial.print(midi_note);
      //Serial.print(" ");
      //Serial.print(midi_vel);
      //Serial.println();
    }

    else if (inByte == 0xB0) // reading for button presses
    {
      delayMicroseconds(550);
      byte midi_button = Serial1.read();
      delayMicroseconds(550);
      bool midi_state = Serial1.read();
      
      Serial.write(255); // midi button initialiser
      if(midi_state)
      {
        Serial.write(midi_button);
      }
      else
      {
        Serial.write(midi_button+128);
      }

      Serial.write(250); // terminator

      //Serial.print("button ");
      //Serial.print(midi_button);
      //if(midi_state){
      //  Serial.println(" down.");
      //}
      //else
      //{
      //  Serial.println(" up.");
      //}  
    }
  }
}

void MidiReadRaw()
{
  if (Serial1.available())
  {
    while (Serial1.available())
    {
      byte inByte = Serial1.read();
      Serial.print(inByte);
      Serial.print(" ");
      delayMicroseconds(550);
    }

    Serial.println(" end.");
  }
  
}

byte MapMidiButton(byte index)
{
  if(index < 9)
  {
    byte res = index - 1;
    return res;
  }

  // other three button banks missing for now
  
}

