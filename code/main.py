# ######################################################################################################################
#                                                  Ultimate Sequencer
# ######################################################################################################################

# add to sys path to enable easy importing
import sys
sys.path.insert(1, '/home/pi/musicon/hardwareio')
sys.path.insert(1, '/home/pi/musicon/hardwareio/i2c_buttons')
sys.path.insert(1, '/home/pi/musicon/hardwareio/leds')

# imports

# python
import serial

# custom
from config import *
import clock
from hardwareio.i2c_buttons.inputgateway import InputGateway
from hardwareio.leds.outputgateway import OutputGateway
from hardwareio.leds.ledmatrix import LEDMatrix
from seqengine import SeqEngine
from midi import MidiController


class Main:
    def __init__(self):
        # ##############################################################################################################
        # ############################################# init hardware###################################################
        print('starting setup')

        # ##### BUTTONS #####
        self.buttons = InputGateway()

        # ##### LEDS #####
        self.led_strip = OutputGateway()                                   # Arduino outputting to leds

        self.led_matrix = LEDMatrix(self.led_strip, 4, 16, 0)

        for i in range(64):
            self.led_matrix.set_pixel(i, 0, 0.6, 1, 1, 2)

        # ##### MIDI #####
        self.midi = MidiController(2)
        self.midi.debug_output_flag = True

        # ##############################################################################################################
        # ############################################# init software # ################################################
        # initialize Seqengines
        self.seqengine = SeqEngine(self.midi, 0, 'testinstr')

        # setting up test seqengine
        self.seqengine.instrument_track.addNoteTrackSinglePitch('kick', 36)
        for i in range(32):
            self.seqengine.instrument_track.note_tracks[0].notes[i*4].setVelocity(100)

        self.seqengine.instrument_track.addNoteTrackSinglePitch('snare', 38)
        for i in range(24):
            self.seqengine.instrument_track.note_tracks[1].notes[i*5].setVelocity(100)

        self.seqengine.instrument_track.addNoteTrackSinglePitch('hihat', 42)
        for i in range(64):
            self.seqengine.instrument_track.note_tracks[2].notes[i*2].setVelocity(100)
        # niiiice pattern for the first time


    # ##############################################################################################################
    # ############################################  main loop  #####################################################
        print('reached main loop.')

        self.clock = clock.Clock(TARGET_FPS, 1)
        self.running = True
        while self.running:
            self.getInputs()
            self.update()
            self.draw()
            self.clock.tick()

    def getInputs(self):
        self.buttons.poll()

        # show button inputs:
        for i in range(64):
            if self.buttons.read_button(i):
                print('button ' + str(i) + ' pressed')

        # show midi notes inputs:
        for i in range (128):
            if self.buttons.read_midi(i):
                print('midinote ' + str(i) + 'vel ' + str(self.buttons.read_midi))


    def update(self):
        self.seqengine.update()

    def draw(self):
        pass
        self.led_matrix.draw()


# start program!
Main()
