import time
import config

class TimeKeeper:
    def __init__(self, seqengine, midi, debug_mode=0):

        self.debug_mode = debug_mode
        self.seqengine = seqengine
        self.midi = midi

        self.tick_length = 60.0 / config.BPM * 0.25

        self.last_tick_time = time.time()
        self.next_tick_time = time.time() + self.tick_length


    def update(self):
        time_to_tick = self.next_tick_time - time.time()
        if time_to_tick < config.CPU_WAIT_FOR_TICK_TIME:
            self.waitForTick()

    def launchTick(self):
        # resetting timers

        if self.debug_mode:
            self.debug_output()

        self.last_tick_time = time.time()
        self.next_tick_time = time.time() + self.tick_length
        self.seqengine.ticked_flag = True # send infop to main not to tick the clock this frame


        self.midi.write() # send notes to midiout.

    def waitForTick(self):

        # access all notetracks to get notes
        for note_track in self.seqengine.instrument_track.note_tracks:
            note = note_track.tick()

            if note:

                if note.velocity == 0:
                    self.midi.add_job(self.seqengine.channel, note_track.pitch, note.velocity, False)

                if note.velocity > 0 and not note_track.mute_flag:
                    self.midi.add_job(self.seqengine.channel, note_track.pitch, note.velocity, True)
        # done writing list

        while time.time() < self.next_tick_time: # wait for the exact timing
            time.sleep(0.00001)

        self.time_at_launch = time.time()
        self.launchTick()

    def debug_output(self):
        # print('last_tick_time:'),
        # print(round(self.last_tick_time,2)),


        print('TIIIIICK!!!!')

        print('calc ticklenght:'),
        print(self.tick_length),

        print('actual tick time:'),
        print(round(self.time_at_launch - self.last_tick_time, 5))