from matrix_config import *


class ButtonMatrix:
    def __init__(self, serial):

        self.serial = serial
        self.button_states = []
        for i in range(128):
            self.button_states.append(False)

        self.analog_reading = []
        for i in range(6):
            self.analog_reading.append(0)

        self.input = []

    def read_button(self, index):
        return self.button_states[BUTTON_PIN_LIST[index]]

    def read_analog(self, index):
        return self.analog_reading[index]

    def poll(self):

        # print('self.serial.in_waiting:' + str(self.serial.in_waiting))
        res = None

        while self.serial.in_waiting > 0 and not res:

            data = self.serial.read()
            if data == 's' or data == 'a':
                self.input = []
                self.input.append(data)

            elif data == 'e':
                res = self.input

            else:
                self.input.append(data)

        if res:
            if res[0] == 's':  # reading buttons
                res.remove('s')
                print(res)
                print(len(res))
                value = 0
                if len(res) == 1:
                    value = int(res[0])

                elif len(res) == 2:
                    value = int(res[0])*10 + int(res[1])

                elif len(res) == 3:
                    value = int(res[0])*100 + int(res[1]) * 10 + int(res[2])

                else:
                    print("error in button input: invalid data structure")

                print(value)

                # print('value: ' + str(value))
                #
                # if self.reading_buttons_flag: #  0 - 128: button down,
                #     self.reading_buttons_flag = False
                #
                #     if 0 <= res < 128:
                #         self.button_states[res] = True
                #         print('button ' + str(res) + ' pressed')
                #
                #     elif 128 < res < 256: # 128 - 256: button up
                #         self.button_states[res - 128] = False
                #         print('button ' + str(res-128) + ' unpressed')
                #
                # elif self.reading_analog_flag:
                #     self.reading_analog_flag = False

