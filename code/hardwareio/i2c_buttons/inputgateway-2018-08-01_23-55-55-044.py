# reads Serial data from arduino with sketch : musicon_button_reader.ino.

# python modules
import serial

# custom modules
from hardwareio.confighwio import *
from hardwareio.i2c_buttons.decode_serial_bytes import decode_serial_bytes

class InputGateway:
    def __init__(self):

        # setting up serial
        self.serial = serial.Serial(SERIAL_OUTPUT_GATEWAY, SERIAL_BAUDRATE)

        # input buffer
        self.input = []


        # initialising all variables to store the input information:
        self.button_states = []
        for i in range(128):
            self.button_states.append(False)

        self.analog_reading = []
        for i in range(6):
            self.analog_reading.append(0)

        self.midi_notes = []
        for i in range(128):
            self.midi_notes.append(0)

        self.midi_button_states = []
        for i in range(32):
            self.midi_button_states.append(False)

    def read_button(self, index):
        return self.button_states[BUTTON_PIN_LIST[index]]

    def read_analog(self, index):
        return self.analog_reading[index]

    def read_midi(self, index):
        return self.midi_notes[index]

    def poll(self):

        message = []

        # read from serial if available
        if self.serial.in_waiting > 0:
            done = False
            while self.serial.in_waiting and not done:
                # reading in one byte
                reading = self.serial.read()
                # converting reading to int. WTF this took me about 10 hours to figure out
                reading = sum([ord(b) << (8 * i) for i, b in enumerate(reading)])
                # append byte to the message
                message.append(reading)

                # checking for stopbyte
                if message[-1] == 250:
                    done = True
                    message.pop(-1) # removing stopbyte

            # decode reading
            if message[0] == 251:
                # analog message
                value = 1 / 249 * message[2]
                self.analog_reading[message[1]] = value

                print('ADC ' + str(message[1]) + ':' + str(value))

            # elif message[0] == 252:

            # elif


        # # print('self.serial.in_waiting:' + str(self.serial.in_waiting))
        # res = None
        #
        # while self.serial.in_waiting > 0 and not res:
        #
        #     message = self.serial.read()
        #     if message == 's' or message == 'a':
        #         self.input = []
        #         self.input.append(message)
        #
        #     elif message == 'e':
        #         res = self.input
        #
        #     else:
        #         self.input.append(message)
        #
        # if res:
        #     if res[0] == 's':  # reading buttons
        #         res.remove('s') # first, remove the message indicator byte
        #
        #         # converting to decimal int
        #         while len(res) < 3:
        #             res.insert(0, '0')
        #
        #         value = int(res[0]) * 100 + int(res[1]) * 10 + int(res[2])
        #
        #         # decoding, writing to buttons
        #         if 0 <= value < 128:
        #             self.button_states[value] = True
        #             # print('button ' + str(value) + ' pressed')
        #
        #         elif 128 < value < 256: # 128 - 256: button up
        #             self.button_states[value - 128] = False
        #             # print('button ' + str(value-128) + ' unpressed')
        #
        #     elif res[0] == 'a': # reading analog ins
        #         res.remove('a') # first, remove the message indicator byte
        #
        #         index = int(res.pop(0)) # read in the index
        #
        #         # converting to decimal
        #         while len(res) < 3:
        #             res.insert(0, '0')
        #
        #         value = int(res[0]) * 100 + int(res[1]) * 10 + int(res[2])
        #
        #         # writing to class message
        #         self.analog_reading[index] = value

            # elif res[0] == 'n': # reading midi note
            #     res.remove('n')
            #
            #     index = int(res.pop(0))
            #
            #     # # converting to decimal
            #     # while len(res) < 3:
            #     #     res.insert(0, '0')
            #     #
            #     # value = int(res[0]) * 100 + int(res[1]) * 10 + int(res[2])
            #
            #     value = self.to_int(res)
            #
            #
            # elif res[0] == 'v': #reading midi velocity
            #     res.remove('v')
            #
            #     #missing


    def decode_serial(self, data):
        res = []
        for d in data:
            if d[0] == '/' and d[1] == 'x':

                temp = 16*int(d[2]) + int(d[3])
                res.append(temp)

            else:
                res.append(int(d))

        return res
