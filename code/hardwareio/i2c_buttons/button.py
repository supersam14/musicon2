from pcf8575 import PCF8575

class Button:
    def __init__(self, address_list):
        if len(address_list) < 1:
            print("Error: empty address list")
        self.address_list = address_list
        self.state = []
        self.pcf8575 = []
        self.chip_count = 0
        for address in address_list:
            self.pcf8575.append(PCF8575(address))
            for i in range(16):
                self.state.append(False)
            self.chip_count += 1

        print(self.chip_count)

    def update(self):
        for pcf in self.pcf8575:
            pcf.update()

        for i in range(self.chip_count):
            for j in range(16):
                self.state[i*16 + j] = self.pcf8575[i].read(j)

    def read(self, index):
        return self.state[index]
