from matrix_config import *


class ButtonMatrix:
    def __init__(self, serial):

        self.serial = serial
        self.button_states = []
        for i in range(128):
            self.button_states.append(False)

        self.analog_reading = []
        for i in range(6):
            self.analog_reading  = []

        self.read_cycle = 0

        self.button_digit_0 = 0
        self.button_digit_1 = 0
        self.button_digit_2 = 0

        self.analog_digit_0 = 0
        self.analog_digit_1 = 0
        self.analog_digit_2 = 0
        self.analog_digit_3 = 0

    def read_button(self, index):
        return self.button_states[BUTTON_PIN_LIST[index]]

    def read_analog(self, index):
        return self.analog_reading[index]

    def poll(self):

        # print('self.serial.in_waiting:' + str(self.serial.in_waiting))

        while self.serial.in_waiting > 0:

                data = self.serial.read()
                print(data)
                #
                # reading_buttons = False
                # reading_analog = False
                #
                # res = self.serial.read_button()
                # if self.read_cycle == 0:
                #     data = self.serial.read_button()
                #     # print('data' + str(data))
                #     if data == 's':
                #         reading_buttons = True
                #         self.read_cycle = 1
                #
                #     elif data == 'a':
                #         reading_analog = True
                #         self.analog_read_cycle = 1
                #
                # elif self.read_cycle == 1:
                #     data = self.serial.read_button()
                #     if data == 'e' and not 'a':
                #         self.read_cycle = 0
                #     else:
                #         self.button_digit_0 = int(data)
                #         self.read_cycle = 2
                #
                # elif self.read_cycle == 2:
                #     data = self.serial.read_button()
                #     if data == 'e' and not 'a':
                #         res = int(self.button_digit_0)
                #         self.read_cycle = 0
                #     else:
                #         self.button_digit_1 = int(data)
                #         self.read_cycle = 3
                #
                # elif self.read_cycle == 3:
                #     data = self.serial.read_button()
                #     if data == 'e' and not 'a':
                #         res = int(self.button_digit_0) * 10 + int(self.button_digit_1)
                #         self.read_cycle = 0
                #     else:
                #         self.button_digit_2 = int(data)
                #         self.read_cycle = 4
                #
                # elif self.read_cycle == 4:
                #     data = self.serial.read_button()
                #     if data == 'e' and not 'a':
                #         res = self.button_digit_0 * 100 + 10 * self.button_digit_1 + self.button_digit_2
                #     self.read_cycle = 0

                # decode data.
                # print(res)
                if res:
                    if self.reading_buttons_flag: #  0 - 128: button down,
                        self.reading_buttons_flag = False

                        if 0 <= res < 128:
                            self.button_states[res] = True
                            print('button ' + str(res) + ' pressed')

                        elif 128 < res < 256: # 128 - 256: button up
                            self.button_states[res - 128] = False
                            print('button ' + str(res-128) + ' unpressed')

                    elif self.reading_analog_flag:
                        self.reading_analog_flag = False

