from matrix_config import *

class ButtonMatrix:
    def __init__(self, serial):

        self.serial = serial
        self.states = []
        for i in range(128):
            self.states.append(False)

        self.read_cycle = 0
        self.digit_0 = 0
        self.digit_1 = 0
        self.digit_2 = 0

    def read(self, index):
        return self.states[BUTTON_PIN_LIST[index]]

    def poll(self):

        # print('self.serial.in_waiting:' + str(self.serial.in_waiting))
        # print('pollin.')

        done = False
        while not done:

            if self.serial.in_waiting > 0:

                reading_buttons = False
                reading_analog = False

                res = self.serial.read()
                if self.read_cycle == 0:
                    data = self.serial.read()
                    # print('data' + str(data))
                    if data == 's':
                        reading_buttons = True
                        self.read_cycle = 1

                    elif data == 'a':
                        reading_analog = True
                        self.read_cycle = 1

                elif self.read_cycle == 1:
                    data = self.serial.read()
                    if data == 'e':
                        self.read_cycle = 0
                    else:
                        self.digit_0 = int(data)
                        self.read_cycle = 2

                elif self.read_cycle == 2:
                    data = self.serial.read()
                    if data == 'e':
                        res = int(self.digit_0)
                        self.read_cycle = 0
                    else:
                        self.digit_1 = int(data)
                        self.read_cycle = 3

                elif self.read_cycle == 3:
                    data = self.serial.read()
                    if data == 'e':
                        res = int(self.digit_0)*10 + int(self.digit_1)
                        self.read_cycle = 0
                    else:
                        self.digit_2 = int(data)
                        self.read_cycle = 4

                elif self.read_cycle == 4:
                    data = self.serial.read()
                    if data == 'e':
                        res = self.digit_0*100 + 10*self.digit_1 + self.digit_2
                    self.read_cycle = 0

                # decode data.
                # print(res)
                if res:
                    if reading_buttons: #  0 - 128: button down,
                        if 0 <= res < 128:
                            self.states[res] = True
                            print('button ' + str(res) + ' pressed')

                        elif 128 < res < 256: # 128 - 256: button up
                            self.states[res-128] = False
                            print('button ' + str(res-128) + ' unpressed')

                    elif reading_analog:  # after 'a' comes the index of the analog pin on arduino, then value.
                                          # add another readcycle maybe to cover values.
                        pass
            else: # stay until nothing more comes
                done = True
