from colorsys import hsv_to_rgb
import time
import serial
from config import *


from helpers import *


class OutputGateway:
    def __init__(self):

        self.serial = serial.Serial(SERIAL_OUTPUT_GATEWAY, SERIAL_BAUDRATE)
        self.check_byte = 255

        time.sleep(1.5) # wait for arduino to boot (1.5s is minimum)

    def write(self, i, h, s, v):

        h = constrain(h, 0, 1)
        s = constrain(s, 0, 1)
        v = constrain(v, 0, 1)

        rgb = hsv_to_rgb(h, s, v)   # converting to rgb format
        r = int(rgb[0]*254.0)       # converting to byte format (0-255)
        g = int(rgb[1]*254.0)
        b = int(rgb[2]*254.0)

        # s = serial.Serial(SERIAL_ARDUINO, SERIAL_BAUDRATE)
        # super duper diy  Serial protocol
        self.serial.write(chr(self.check_byte))
        self.serial.write(chr(i))
        self.serial.write(chr(r))
        self.serial.write(chr(g))
        self.serial.write(chr(b))
