from animation import Animation


class NPixel:
    def __init__(self, strip, index):

        self.strip = strip
        self.index = index

        self.hue = Animation()
        self.saturation = Animation()
        self.brightness = Animation()

        self.last_hue = 0
        self.last_sat = 0
        self.last_brightness = 0

    def draw(self):
        # pass

        self.last_hue = self.hue.value
        self.last_sat = self.saturation.value
        self.last_brightness = self.brightness.value

        hue = self.hue.animate()
        sat = self.saturation.animate()
        val = self.brightness.animate()

        go_flag = False
        # if self.index == 0:
        #     print'last color:', str(hue), str(sat), str(val),
        #     print'color now:', str(self.last_hue), str(self.last_sat), str(self.last_brightness)
        #

        if hue != self.last_hue:
            go_flag = True

        if sat != self.last_sat:
            go_flag = True

        if val != self.last_brightness:
            go_flag = True

        # only send if changed
        # if (self.last_brightness != hue) or (self.last_sat != sat) or (self.last_brightness != val):
        if go_flag:
            self.strip.write(self.index, hue, sat, val)
            # self.strip.write(self.index, 0, 1, 0.5)
            # if self.index == 0:
            #     print('sending')
