from colorsys import hsv_to_rgb


class NPixelStrip:
    def __init__(self, bus,  address):

        self.address = address
        self.bus = bus
        self.write(0, 0, 255, 255)

    def write(self, i, h, s, v):
        # pass
        if h > 1:
            h = 1
        if h < 0:
            h = 0
        if s > 1:
            s = 1
        if s < 0:
            s = 0
        if v > 1:
            v = 1
        if v < 0:
            v = 0

        rgb = hsv_to_rgb(h, s, v)   # converting to rgb format
        r = int(rgb[0]*255.0)       # converting to byte format (0-255)
        g = int(rgb[1]*255.0)
        b = int(rgb[2]*255.0)

        # send to the i2c bus
        self.bus.write_byte(self.address, 255)
        self.bus.write_byte(self.address, i)
        self.bus.write_byte(self.address, r)
        self.bus.write_byte(self.address, g)
        self.bus.write_byte(self.address, b)
