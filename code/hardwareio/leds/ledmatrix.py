from npixel import NPixel


class LEDMatrix:
    def __init__(self, strip, cols, rows, starting_index):

        self.strip = strip
        self.starting_index = starting_index
        self.rows = rows
        self.cols = cols

        self.matrix = []
        for i in range(rows*cols):
            self.matrix.append(NPixel(strip, i))

# use this to draw in main loop
    def draw(self):
        for pixel in self.matrix:
            pixel.draw()

# set pixels here
    def set_pixel(self, x, y, h, s, v, ramptime):
        index = x + (self.rows * y) + self.starting_index
        self.matrix[index].hue.value = h
        self.matrix[index].saturation.value = s
        self.matrix[index].brightness.set_anim(v, ramptime)
