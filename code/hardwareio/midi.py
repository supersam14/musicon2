# -------------------------MIDI-----------------------------------------------

import pygame
import pygame.midi

class MidiController:
    def __init__(self, device_id=2):
        self.instrument = 0
        self.channel = 1
        self.volume = 127

        self.debug_output_flag = False

        # init pygame
        pygame.init()
        pygame.midi.init()

        self.device_id = device_id # usually 2 with midi usb dongle
        # create midi output object
        self.midiOutput = pygame.midi.Output(device_id)  # TODO: move to update and delete everytime. Check timing

        self.queue = [] # allocating memory for the jobqueue

        # self.midiOutput.set_instrument(instrument) # unclear about function

        # doesnt work with same id
        # self.midiInput = pygame.midi.Input(device_id)

    def print_device_info(self):
        print()
        print('number of midi devices:'),
        print(pygame.midi.get_count())

        for id in range(pygame.midi.get_count()):
            print('device: '),
            print(pygame.midi.get_device_info(id))
            print()

    # add job to queue
    def add_job(self, channel, note, velocity, on_off_flag):
        self.queue.append(ToneJob(channel, note, velocity, on_off_flag))

    # send all jobs to midi device
    def write(self):

        for tone in self.queue:
            if tone.flag: # note on
                self.midiOutput.note_on(tone.note, tone.velocity, tone.channel)
            else:
                self.midiOutput.note_off(tone.note, tone.velocity, tone.channel)

        # delete the job-qeue
        while(len(self.queue)>0):
            self.queue.pop()


# used for queuing midi write jobs
class ToneJob:
    def __init__(self, channel, note, velocity, on_off_flag):
        self.channel = channel
        self.note = note
        self.velocity = velocity
        self.flag = on_off_flag
