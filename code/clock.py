import time

class Clock:
    def __init__(self, FPS, debug_level=0):

        self.fps = FPS
        self.debug_level = debug_level

        self.last_clock_time = time.time()
        self.clock_cycle_length = 1.0 / FPS
        self.next_clock_time = time.time() + self.clock_cycle_length
        self.started_waiting = time.time()
        self.sleep_dur = 0

        self.debug_counter = 0

    def tick(self):
        self.started_waiting = time.time()
        self.sleep_dur = self.next_clock_time - self.started_waiting

        if self.sleep_dur < 0:
            self.sleep_dur = 0

        # print('sleep dur:'),
        # print(self.sleep_dur)

        time.sleep(self.sleep_dur)

        self.cpu_work_time = self.started_waiting - self.last_clock_time

        self.measured_clock_time = time.time() - self.last_clock_time

        self.last_clock_time = time.time()
        self.next_clock_time = time.time() + self.clock_cycle_length

# debug print all 50 frames
        self.debug_counter += 1
        if self.debug_counter > 50:
            self.debug_counter = 0 # reset counter

            if self.debug_level == 1:  # print depending on debug level
                self.lightDebug()

            if self.debug_level == 2:
                self.heavyDebug()

    def lightDebug(self):
        print('FPS:'),
        print(round(1 / self.measured_clock_time, 2)),

        idle_time = (time.time() - self.started_waiting)
        free_cpu_capacity = idle_time / (idle_time + self.cpu_work_time) * 100

        print('Free CPU capacity:'),
        print(round(free_cpu_capacity, 2)),
        print('%'),

        print(' ')

    def heavyDebug(self):
        print('FPS:'),
        print(1 / self.measured_clock_time),
        #
        print('Target FPS:'),
        print(self.fps),
        #

        print('meas clk time:'),
        print(round(self.measured_clock_time * 1000, 2)),
        #
        print('target clk time:'),
        print(round(self.clock_cycle_length * 1000, 2))
        #

        print('sleeptime'),
        print(round(self.sleep_time * 1000, 2)),

        print('cpu work time:'),
        print(round(self.cpu_work_time * 1000, 2)),
        #
        idle_time = (time.time() - self.started_waiting)
        print('cpu idle time:'),
        print(round(idle_time * 1000, 2))
