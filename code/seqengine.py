from timekeeper import TimeKeeper
from instrumenttrack import InstrumentTrack


class SeqEngine:
    def __init__(self, midi, channel, name):

        self.midi = midi

        self.name = name
        self.channel = channel

        self.time_keeper = TimeKeeper(self, self.midi)
        self.instrument_track = InstrumentTrack(name)

    def update(self):
        self.time_keeper.update()

    def draw(self):
        pass
